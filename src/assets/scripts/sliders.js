import Swiper from 'swiper/swiper-bundle';

if ($('.gallery-top').length > 0) {
	const galleryTop = new Swiper('.gallery-top', {
		spaceBetween: 10,
		slidesPerView: 1,
		simulateTouch: false,
		centeredSlides: true,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
	});

	$('.gallery-thumbs .thumbs-item').click(function() {
		$('.thumbs-item').removeClass('active');
	  const index = $(this).data('slide');
	  const video = $('.video');
		$(this).addClass('active');
		galleryTop.slideTo(index);
		video[0].pause();
	});
}

if ($('.typicalSlider').length > 0) {
	const swiperInstances = [];
	$('.typicalSlider').each(function(index) {
		const $this = $(this);
		const $slider = $(this)[0];
		const sliders = $this.data('slider');
		const slidersForTab = $this.data('tab-slider');
		const columnForTab = $this.data('tab-column');
		const prev = $this.parent().find('.swiper-prev')[0];
		const next = $this.parent().find('.swiper-next')[0];
		const options = {
			slidesPerView: 1,
			slidesPerColumn: columnForTab || 1,
			spaceBetween: 10,
			loop: !columnForTab,
			navigation: {
				nextEl: next,
				prevEl: prev,
			},
			breakpoints: {
				1200: {
					spaceBetween: 30,
					slidesPerView: sliders || 1,
					slidesPerColumn: 1,
					loop: true,
				},
				768: {
					spaceBetween: 30,
					slidesPerView: slidersForTab || 1,
					slidesPerColumn: 1,
					loop: true,
				}
			},
		};

		swiperInstances[index] = new Swiper($slider, options);

		$(window).on('orientationchange', function() {
			setTimeout(() => {
				swiperInstances[index].update();
			}, 800);
		});
	});
}
